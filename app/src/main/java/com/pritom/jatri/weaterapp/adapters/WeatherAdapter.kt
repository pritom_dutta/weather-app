package com.pritom.jatri.weaterapp.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pritom.jatri.network.model.weather.WeatherModel
import com.pritom.jatri.weaterapp.databinding.ListItemBinding
import com.pritom.jatri.weaterapp.interfaces.OnClickItem

class WeatherAdapter : RecyclerView.Adapter<MyViewHolder>() {
    private val mWeatherModelList = ArrayList<WeatherModel>()
    private lateinit var mOnClickItem: OnClickItem

    fun setList(movies: List<WeatherModel>) {
        mWeatherModelList.clear()
        mWeatherModelList.addAll(movies)
        notifyDataSetChanged()
    }

    fun setListener(mOnClickItem: OnClickItem) {
        this.mOnClickItem = mOnClickItem
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemBinding =
            ListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        return mWeatherModelList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(mWeatherModelList[position])
        holder.binding.root.setOnClickListener {
            if (mOnClickItem != null) {
                mOnClickItem.clickItem(mWeatherModelList[position])
            }
        }
    }


}

class MyViewHolder(val binding: ListItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(mWeatherModel: WeatherModel) {
        binding.tvName.text = mWeatherModel.name
        binding.tvTemp.text = mWeatherModel?.main?.temp?.let { kalvinToCel(it).toString() + "°C" }
        binding.tvWeatherStatus.text = mWeatherModel.weather?.get(0)?.main
    }

    private fun kalvinToCel(temp: Double): Double {
        val value = String.format("%.1f", (temp - 273.15)).toDouble()
        return value
    }
}

