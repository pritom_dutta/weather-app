package com.pritom.jatri.weaterapp.interfaces

import com.pritom.jatri.network.model.weather.WeatherModel

interface OnClickItem {
    fun clickItem(weatherModel: WeatherModel);
}