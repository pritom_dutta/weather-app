package com.pritom.jatri.weaterapp.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.pritom.jatri.network.model.weather.WeatherModel
import com.pritom.jatri.weaterapp.R
import com.pritom.jatri.weaterapp.ui.MainActivity

class NotificationService : Service() {
    private val CHANNEL_ID = "ForegroundService Kotlin"

    companion object {
        fun startService(context: Context, name: String, temp: String) {
            val startIntent = Intent(context, NotificationService::class.java)
            startIntent.putExtra("name", name)
            startIntent.putExtra("temp", temp)
            ContextCompat.startForegroundService(context, startIntent)
        }

        fun stopService(context: Context) {
            val stopIntent = Intent(context, NotificationService::class.java)
            context.stopService(stopIntent)
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        //do heavy work on a background thread
        val name = intent?.getStringExtra("name")
        val temp = intent?.getStringExtra("temp")
        createNotificationChannel()
        val notificationIntent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(
            this,
            0, notificationIntent, 0
        )

        val smallContent = RemoteViews(getPackageName(), R.layout.small_layout_notification)
        val bigContent = RemoteViews(getPackageName(), R.layout.small_layout_notification)

        bigContent.setTextViewText(R.id.notification_title, name)
        smallContent.setTextViewText(R.id.notification_title, name)

        bigContent.setTextViewText(R.id.notification_sub, temp)
        smallContent.setTextViewText(R.id.notification_sub, temp)

        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentIntent(pendingIntent)
            .setContent(smallContent)
            .setPriority(NotificationManager.IMPORTANCE_LOW)
            .setCustomBigContentView(bigContent)
            .build()
        startForeground(1, notification)
        //stopSelf();
        return START_NOT_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                CHANNEL_ID, "Foreground Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val manager = getSystemService(NotificationManager::class.java)
            manager!!.createNotificationChannel(serviceChannel)
        }
    }

    private fun kalvinToCel(temp: Double): Double {
        val value = String.format("%.1f", (temp - 273.15)).toDouble()
        return value
    }
}
