package com.pritom.jatri.weaterapp.ui

import android.app.Application
import com.pritom.jatri.basicmovie.presentation.di.Injector
import com.pritom.jatri.basicmovie.presentation.di.core.*
import com.pritom.jatri.basicmovie.presentation.di.weather.WeatherSubComponent
import com.pritom.jatri.network.BuildConfig

class App: Application(),Injector{

    private lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(applicationContext))
            .netModule(NetModule(BuildConfig.BASE_URL))
            .remoteDataModule(RemoteDataModule(BuildConfig.API_KEY,"23.68","90.35","50"))
            .build()
    }



    override fun createWeatherSubComponent(): WeatherSubComponent {
        return appComponent.weatherSubComponent().create()
    }
}