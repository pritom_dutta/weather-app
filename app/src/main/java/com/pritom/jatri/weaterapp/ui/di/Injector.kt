package com.pritom.jatri.basicmovie.presentation.di

import com.pritom.jatri.basicmovie.presentation.di.weather.WeatherSubComponent


interface Injector {
    fun createWeatherSubComponent(): WeatherSubComponent
}