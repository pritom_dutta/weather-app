package com.pritom.jatri.basicmovie.presentation.di.core

import com.pritom.jatri.basicmovie.presentation.di.weather.WeatherSubComponent
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        NetModule::class,
        DataBaseModule::class,
        UseCaseModule::class,
        RemoteDataModule::class,
        RepositoryModel::class,
        LocalDataModule::class,
        CacheDataModule::class,
    ]
)
interface AppComponent {
    fun weatherSubComponent(): WeatherSubComponent.Factory
}