package com.pritom.jatri.basicmovie.presentation.di.core

import android.content.Context
import com.pritom.jatri.basicmovie.presentation.di.weather.WeatherSubComponent
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module(subcomponents = [WeatherSubComponent::class])
class AppModule(private val context: Context) {

    @Singleton
    @Provides
    fun provideApplicationContext():Context{
        return context.applicationContext
    }
}