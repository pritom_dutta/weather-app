package com.pritom.jatri.basicmovie.presentation.di.core

import com.pritom.jatri.basicmovie.network.repository.movie.datasource.WeatherCacheDataSource
import com.pritom.jatri.basicmovie.network.repository.movie.datasourceImpl.WeatherCacheDataSourceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class CacheDataModule {

    @Singleton
    @Provides
    fun provideWeatherCacheDataSource(): WeatherCacheDataSource {
        return WeatherCacheDataSourceImpl()
    }

}