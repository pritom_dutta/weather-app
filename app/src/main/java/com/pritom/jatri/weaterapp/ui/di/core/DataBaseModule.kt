package com.pritom.jatri.basicmovie.presentation.di.core

import android.content.Context
import androidx.room.Room
import com.pritom.jatri.basicmovie.network.db.TMDBdatabase
import com.pritom.jatri.basicmovie.network.db.dao.WeatherDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataBaseModule {

    @Singleton
    @Provides
    fun providerTMDBDataBase(context: Context):TMDBdatabase{
        return Room.databaseBuilder(context,TMDBdatabase::class.java,"jatridb").build()
    }

    @Singleton
    @Provides
    fun providerWeather(tmdBdatabase: TMDBdatabase):WeatherDao{
        return tmdBdatabase.weatherDao()
    }
}