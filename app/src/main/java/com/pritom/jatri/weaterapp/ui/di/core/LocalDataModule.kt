package com.pritom.jatri.basicmovie.presentation.di.core

import com.pritom.jatri.basicmovie.network.db.dao.WeatherDao
import com.pritom.jatri.basicmovie.network.repository.movie.datasource.WeatherLocalDataSource
import com.pritom.jatri.basicmovie.network.repository.movie.datasourceImpl.WeatherLocalDataSourceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class LocalDataModule() {

    @Singleton
    @Provides
    fun provideWeatherLocalDataSource(weatherDao: WeatherDao): WeatherLocalDataSource {
        return WeatherLocalDataSourceImpl(weatherDao)
    }
}