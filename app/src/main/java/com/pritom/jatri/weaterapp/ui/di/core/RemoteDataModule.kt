package com.pritom.jatri.basicmovie.presentation.di.core

import com.pritom.jatri.basicmovie.network.api.TMDBService
import com.pritom.jatri.basicmovie.network.repository.movie.datasource.WeatherRemoteDataSource
import com.pritom.jatri.basicmovie.network.repository.movie.datasourceImpl.WeatherRemoteDataSourceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RemoteDataModule(
    private val apiKey: String,
    private val lat: String,
    private val lon: String,
    private val cnt: String
) {

    @Singleton
    @Provides
    fun provideWeatherRemoteDataSource(tmbdService: TMDBService): WeatherRemoteDataSource {
        return WeatherRemoteDataSourceImpl(tmbdService, apiKey, lat, lon, cnt)
    }
}