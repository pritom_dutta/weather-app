package com.pritom.jatri.basicmovie.presentation.di.core

import com.pritom.jatri.basicmovie.domain.movieusecase.WeatherRepository
import com.pritom.jatri.basicmovie.network.repository.movie.WeatherRepositoryImpl
import com.pritom.jatri.basicmovie.network.repository.movie.datasource.WeatherCacheDataSource
import com.pritom.jatri.basicmovie.network.repository.movie.datasource.WeatherLocalDataSource
import com.pritom.jatri.basicmovie.network.repository.movie.datasource.WeatherRemoteDataSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModel {

    @Singleton
    @Provides
    fun provideMovieRepository(
        weatherRemoteDataSource: WeatherRemoteDataSource,
        weatherLocalDataSource: WeatherLocalDataSource,
        weatherCacheDataSource: WeatherCacheDataSource
    ): WeatherRepository {
        return WeatherRepositoryImpl(
            weatherRemoteDataSource,
            weatherLocalDataSource,
            weatherCacheDataSource
        )
    }
}