package com.pritom.jatri.basicmovie.presentation.di.core


import com.pritom.jatri.basicmovie.domain.movieusecase.GetWeatherUseCase
import com.pritom.jatri.basicmovie.domain.movieusecase.UpdateWeatherUseCase
import com.pritom.jatri.basicmovie.domain.movieusecase.WeatherRepository
import dagger.Module
import dagger.Provides

@Module
class UseCaseModule {

//  Movie Show
    @Provides
    fun providerGetWeatherUseCase(weatherRepository: WeatherRepository): GetWeatherUseCase{
        return GetWeatherUseCase(weatherRepository)
    }

    @Provides
    fun providerUpdateWeatherUseCase(weatherRepository: WeatherRepository): UpdateWeatherUseCase{
        return UpdateWeatherUseCase(weatherRepository)
    }
}