package com.pritom.jatri.basicmovie.presentation.di.weather

import com.pritom.jatri.basicmovie.domain.movieusecase.GetWeatherUseCase
import com.pritom.jatri.basicmovie.domain.movieusecase.UpdateWeatherUseCase
import com.pritom.jatri.weaterapp.ui.home.MainViewModeFactory
import dagger.Module
import dagger.Provides


@Module
class WeatherModule {
    @WeatherScope
    @Provides
    fun providerMovieViewModelFactory(
        getMovieUseCase: GetWeatherUseCase,
        updateMoviesUseCase: UpdateWeatherUseCase
    ): MainViewModeFactory {
        return MainViewModeFactory(getMovieUseCase, updateMoviesUseCase)
    }
}