package com.pritom.jatri.basicmovie.presentation.di.weather

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class WeatherScope()
