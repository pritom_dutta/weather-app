package com.pritom.jatri.basicmovie.presentation.di.weather

import com.pritom.jatri.weaterapp.ui.MainActivity
import com.pritom.jatri.weaterapp.ui.home.MainFragment
import dagger.Subcomponent

@WeatherScope
@Subcomponent(modules = [WeatherModule::class])
interface WeatherSubComponent {
    fun inject(mainFragment: MainFragment)
    fun inject(mainActivity: MainActivity)

    @Subcomponent.Factory
    interface Factory{
        fun create():WeatherSubComponent
    }
}