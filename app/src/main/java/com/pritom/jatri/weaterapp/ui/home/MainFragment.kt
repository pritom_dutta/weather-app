package com.pritom.jatri.weaterapp.ui.home

import android.location.Location
import android.location.LocationManager
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.pritom.jatri.basicmovie.presentation.di.Injector
import com.pritom.jatri.network.model.weather.WeatherModel
import com.pritom.jatri.weaterapp.R
import com.pritom.jatri.weaterapp.adapters.WeatherAdapter
import com.pritom.jatri.weaterapp.databinding.MainFragmentBinding
import com.pritom.jatri.weaterapp.interfaces.OnClickItem
import javax.inject.Inject

class MainFragment : Fragment(), OnClickItem {

    private lateinit var binding: MainFragmentBinding
    private lateinit var mWeatherAdapter: WeatherAdapter

    @Inject
    lateinit var factory: MainViewModeFactory
    private lateinit var mainViewModel: MainViewModel

    companion object {
        fun newInstance() = MainFragment()
    }

//    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(inflater)
        setUp()
        return binding.root
    }

    private fun setUp() {
        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.weatherRecyclerView.layoutManager = layoutManager
        mWeatherAdapter = WeatherAdapter()
        mWeatherAdapter.setListener(this)
        binding.weatherRecyclerView.adapter = mWeatherAdapter

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity?.application as Injector).createWeatherSubComponent().inject(this)

        mainViewModel = ViewModelProvider(this, factory).get(MainViewModel::class.java)
        val responsiveLiveData = mainViewModel.getWeatherList()
        responsiveLiveData.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                mWeatherAdapter.setList(it)
            } else {
                Toast.makeText(context, "No Data", Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun clickItem(weatherModel: WeatherModel) {

        val bundle: Bundle = bundleOf("weatherModel" to weatherModel)
        binding.root.findNavController()
            .navigate(R.id.action_mainFragment_to_weaterDetailsFragment,bundle)
    }
}