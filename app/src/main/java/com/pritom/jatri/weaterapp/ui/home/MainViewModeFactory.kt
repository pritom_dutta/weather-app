package com.pritom.jatri.weaterapp.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pritom.jatri.basicmovie.domain.movieusecase.GetWeatherUseCase
import com.pritom.jatri.basicmovie.domain.movieusecase.UpdateWeatherUseCase

class MainViewModeFactory(
    private val getWeatherUseCase: GetWeatherUseCase,
    private val updateWeatherUseCase: UpdateWeatherUseCase
): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MainViewModel(getWeatherUseCase, updateWeatherUseCase) as T
    }
}