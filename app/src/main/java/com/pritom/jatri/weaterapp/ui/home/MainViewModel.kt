package com.pritom.jatri.weaterapp.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.pritom.jatri.basicmovie.domain.movieusecase.GetWeatherUseCase
import com.pritom.jatri.basicmovie.domain.movieusecase.UpdateWeatherUseCase

class MainViewModel(
    private val getWeatherUseCase: GetWeatherUseCase,
    private val updateWeatherUseCase: UpdateWeatherUseCase
) : ViewModel() {

    fun getWeatherList() = liveData {
        val weatherList = getWeatherUseCase.execute()
        emit(weatherList)
    }

    fun getWeatherDetails(lat:String,ln:String) = liveData {
        val weatherList = getWeatherUseCase.executeTwo(lat,ln)
        emit(weatherList)
    }

    fun updateWeatherList() = liveData {
        val weatherList = updateWeatherUseCase.execute()
        emit(weatherList)
    }
}