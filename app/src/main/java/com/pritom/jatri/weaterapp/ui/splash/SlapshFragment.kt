package com.pritom.jatri.weaterapp.ui.splash

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import com.pritom.jatri.weaterapp.R
import com.pritom.jatri.weaterapp.databinding.FragmentSlapshBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import okhttp3.Dispatcher
import java.util.*
import kotlin.concurrent.schedule


class SlapshFragment : Fragment() {

    private lateinit var binding: FragmentSlapshBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentSlapshBinding.inflate(inflater)
        setUp()
        return binding.root
    }

    private fun setUp() {
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            binding.root.findNavController()
                .navigate(R.id.action_slapshFragment_to_mainFragment)
        }, 3000)
    }
}
