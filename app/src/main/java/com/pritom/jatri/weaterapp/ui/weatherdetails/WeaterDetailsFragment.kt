package com.pritom.jatri.weaterapp.ui.weatherdetails

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.pritom.jatri.network.model.weather.WeatherModel
import com.pritom.jatri.weaterapp.R
import com.pritom.jatri.weaterapp.databinding.FragmentWeaterDetailsBinding
import com.pritom.jatri.weaterapp.databinding.MainFragmentBinding


class WeaterDetailsFragment : Fragment(), OnMapReadyCallback {

    private lateinit var binding: FragmentWeaterDetailsBinding
    private lateinit var weatherModel: WeatherModel
    private lateinit var mMap: GoogleMap
    var mapFragment : SupportMapFragment?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentWeaterDetailsBinding.inflate(inflater)
        weatherModel = requireArguments().getParcelable<WeatherModel>("weatherModel")!!
        setUp()
        return binding.root
    }

    private fun setUp() {

        mapFragment = childFragmentManager.findFragmentById(R.id.fallasMap) as SupportMapFragment
        mapFragment?.getMapAsync(this)

        binding.imgBack.setOnClickListener {
            it.findNavController().popBackStack()
        }

        if(weatherModel != null){
            binding.tvName.text = weatherModel.name
            binding.tvSky.text = weatherModel.weather?.get(0)?.main
            binding.tvWide.text = "Wide Speed: "+weatherModel?.wind?.speed.toString()
            binding.tvHumidity.text = "Humidity: "+weatherModel?.main?.humidity.toString()
            binding.tvMaxTemp.text = "Max Temp.: "+weatherModel?.main?.tempMax?.let { kalvinToCel(it).toString() + "°C" }
            binding.tvMinTemp.text = "Min Temp.: "+weatherModel?.main?.tempMin?.let { kalvinToCel(it).toString() + "°C" }
            binding.tvTemp.text = weatherModel?.main?.temp?.let { kalvinToCel(it).toString() + "°C" }
        }
    }

    private fun kalvinToCel(temp: Double): Double {
        val value = String.format("%.1f", (temp - 273.15)).toDouble()
        return value
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap!!
        if(weatherModel != null) {
            // Add a marker in Sydney and move the camera
            var lat = weatherModel?.coord?.lat
            var lon = weatherModel?.coord?.lon
            val area = LatLng(lat!!, lon!!)
            mMap.addMarker(MarkerOptions().position(area).title(weatherModel.name))
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(area, 15f))
        }

    }

}