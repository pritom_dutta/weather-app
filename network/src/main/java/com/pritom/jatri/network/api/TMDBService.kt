package com.pritom.jatri.basicmovie.network.api

import com.pritom.jatri.network.model.WeatherList
import com.pritom.jatri.network.model.weather.Weather
import com.pritom.jatri.network.model.weather.WeatherModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface TMDBService {

    @GET("data/2.5/find")
    suspend fun getWeather(
        @Query("appid") apiKey: String,
        @Query("lat") lat: String,
        @Query("lon") lon: String,
        @Query("cnt") cnt: String,
    ): Response<WeatherList>

    @GET("data/2.5/weather")
    suspend fun getWeatherDetails(
        @Query("appid") apiKey: String,
        @Query("lat") lat: String,
        @Query("lon") lon: String
    ): Response<WeatherModel>
}