package com.pritom.jatri.basicmovie.network.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.pritom.jatri.basicmovie.network.db.dao.WeatherDao
import com.pritom.jatri.network.model.typeCon.CoordConverter
import com.pritom.jatri.network.model.typeCon.ListConverter
import com.pritom.jatri.network.model.typeCon.MainConverter
import com.pritom.jatri.network.model.typeCon.WindConverter
import com.pritom.jatri.network.model.weather.WeatherModel

@Database(
    entities = [WeatherModel::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(*[MainConverter::class, ListConverter::class, CoordConverter::class, WindConverter::class])
abstract class TMDBdatabase : RoomDatabase() {
    abstract fun weatherDao(): WeatherDao

}