package com.pritom.jatri.basicmovie.network.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.pritom.jatri.network.model.weather.Weather
import com.pritom.jatri.network.model.weather.WeatherModel

@Dao
interface WeatherDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveWeatherList(movies: List<WeatherModel>)

    @Query("DELETE FROM weather_data")
    suspend fun deleteAllWeatherList();

    @Query("SELECT * FROM weather_data")
    suspend fun getWeather(): List<WeatherModel>
}