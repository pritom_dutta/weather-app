package com.pritom.jatri.basicmovie.domain.movieusecase

import com.pritom.jatri.network.model.weather.WeatherModel


class GetWeatherUseCase(private val weatherRepository: WeatherRepository) {
    suspend fun execute(): List<WeatherModel>? = weatherRepository.getWeatherList()
    suspend fun executeTwo(lat:String,ln:String): WeatherModel? = weatherRepository.getWeatherDetails(lat,ln)
}