package com.pritom.jatri.basicmovie.domain.movieusecase

import com.pritom.jatri.network.model.weather.WeatherModel


class UpdateWeatherUseCase (private val weatherRepository: WeatherRepository) {
    suspend fun execute():List<WeatherModel>? = weatherRepository.updateWeatherList()
}