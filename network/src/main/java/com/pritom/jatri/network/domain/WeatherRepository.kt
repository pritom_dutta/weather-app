package com.pritom.jatri.basicmovie.domain.movieusecase

import com.pritom.jatri.network.model.weather.WeatherModel


interface WeatherRepository {
    suspend fun getWeatherList(): List<WeatherModel>?
    suspend fun getWeatherDetails(lat:String,ln:String): WeatherModel?
    suspend fun updateWeatherList(): List<WeatherModel>?
}