package com.pritom.jatri.network.model


import com.google.gson.annotations.SerializedName
import com.pritom.jatri.network.model.weather.WeatherModel

data class WeatherList(
    @SerializedName("list")
    val list: List<WeatherModel>,
    @SerializedName("message")
    val message: String
)