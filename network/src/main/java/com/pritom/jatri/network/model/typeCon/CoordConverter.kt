package com.pritom.jatri.network.model.typeCon

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.pritom.jatri.network.model.weather.Coord

class CoordConverter {
    var gson = Gson()

    @TypeConverter
    fun coordToString(mCoord: Coord): String {
        return gson.toJson(mCoord)
    }

    @TypeConverter
    fun stringToCoord(data: String): Coord {
        val listType = object : TypeToken<Coord>() {
        }.type
        return gson.fromJson(data, listType)
    }
}