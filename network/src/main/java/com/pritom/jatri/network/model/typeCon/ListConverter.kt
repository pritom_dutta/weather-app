package com.pritom.jatri.network.model.typeCon

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.pritom.jatri.network.model.weather.Weather

class ListConverter {
    var gson = Gson()

    @TypeConverter
    fun foodItemToString(foodItems: List<Weather>): String {
        return gson.toJson(foodItems)
    }

    @TypeConverter
    fun stringToFoodItem(data: String): List<Weather> {
        val listType = object : TypeToken<List<Weather>>() {
        }.type
        return gson.fromJson(data, listType)
    }
}