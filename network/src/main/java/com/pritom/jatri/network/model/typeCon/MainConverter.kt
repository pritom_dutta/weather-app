package com.pritom.jatri.network.model.typeCon

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.pritom.jatri.network.model.weather.Main

class MainConverter {
    var gson = Gson()

    @TypeConverter
    fun mainToString(mMain: Main): String {
        return gson.toJson(mMain)
    }

    @TypeConverter
    fun stringToMain(data: String): Main {
        val listType = object : TypeToken<Main>() {
        }.type
        return gson.fromJson(data, listType)
    }
}