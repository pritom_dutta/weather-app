package com.pritom.jatri.network.model.typeCon

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.pritom.jatri.network.model.weather.Wind

class WindConverter {
    var gson = Gson()

    @TypeConverter
    fun mWindToString(mWind: Wind): String {
        return gson.toJson(mWind)
    }

    @TypeConverter
    fun stringToWind(data: String): Wind {
        val listType = object : TypeToken<Wind>() {
        }.type
        return gson.fromJson(data, listType)
    }
}