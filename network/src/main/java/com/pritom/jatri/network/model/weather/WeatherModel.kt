package com.pritom.jatri.network.model.weather


import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "weather_data")

data class WeatherModel(

    @PrimaryKey
    @SerializedName("id")
    val id: Int,
    @SerializedName("dt")
    val dt: Int?,
    @SerializedName("main")
    val main: Main?,
    @SerializedName("coord")
    val coord: Coord?,
    @SerializedName("wind")
    val wind: Wind?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("weather")
    val weather: List<Weather>?
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        TODO("main"),
        TODO("coord"),
        TODO("wind"),
        parcel.readString(),
        TODO("weather")
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeValue(dt)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<WeatherModel> {
        override fun createFromParcel(parcel: Parcel): WeatherModel {
            return WeatherModel(parcel)
        }

        override fun newArray(size: Int): Array<WeatherModel?> {
            return arrayOfNulls(size)
        }
    }
}