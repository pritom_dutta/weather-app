package com.pritom.jatri.basicmovie.network.repository.movie

import android.util.Log
import com.pritom.jatri.basicmovie.domain.movieusecase.WeatherRepository
import com.pritom.jatri.basicmovie.network.repository.movie.datasource.WeatherCacheDataSource
import com.pritom.jatri.basicmovie.network.repository.movie.datasource.WeatherLocalDataSource
import com.pritom.jatri.basicmovie.network.repository.movie.datasource.WeatherRemoteDataSource
import com.pritom.jatri.network.model.weather.WeatherModel
import java.lang.Exception

class WeatherRepositoryImpl(
    private val weatherRemoteDatasource: WeatherRemoteDataSource,
    private val weatherLocalDataSource: WeatherLocalDataSource,
    private val weatherCacheDataSource: WeatherCacheDataSource
) : WeatherRepository {

    override suspend fun getWeatherList(): List<WeatherModel>? {
        return getWeatherListFromCache()
    }

    override suspend fun getWeatherDetails(lat:String,ln:String): WeatherModel? {
        return getWeatherDetailsFromAPI(lat,ln)
    }

    override suspend fun updateWeatherList(): List<WeatherModel>? {
        val newListOfMovies = getWeatherListFromAPI()
        weatherLocalDataSource.clearAll()
        weatherLocalDataSource.saveWeatherFromDB(newListOfMovies)
        weatherCacheDataSource.saveWeatherFromCache(newListOfMovies)
        return newListOfMovies
    }

    suspend fun getWeatherDetailsFromAPI(lat:String,ln:String): WeatherModel{
        lateinit var weatherDetails: WeatherModel
        try {
            val response = weatherRemoteDatasource.getWeatherDetails(lat,ln)
            val body = response.body()
            if (body != null) {
                weatherDetails = body
            }
        } catch (exception: Exception) {
            Log.i("MyTag", exception.message.toString())
        }
        return weatherDetails
    }

    suspend fun getWeatherListFromAPI(): List<WeatherModel> {
        lateinit var weatherList: List<WeatherModel>
        try {
            val response = weatherRemoteDatasource.getWeatherList()
            val body = response.body()
            if (body != null) {
                weatherList = body.list
            }
        } catch (exception: Exception) {
            Log.i("MyTag", exception.message.toString())
        }
        return weatherList
    }

    suspend fun getWeatherListFromDB(): List<WeatherModel> {
        lateinit var weatherList: List<WeatherModel>
        try {
            weatherList = weatherLocalDataSource.getWeatherFromDB()
        } catch (exception: Exception) {
            Log.i("MyTag", exception.message.toString())
        }
        if (weatherList.size > 0) {
            return weatherList
        } else {
            weatherList = getWeatherListFromAPI()
            weatherLocalDataSource.saveWeatherFromDB(weatherList)
        }

        return weatherList
    }

    suspend fun getWeatherListFromCache(): List<WeatherModel> {
        lateinit var weatherList: List<WeatherModel>
        try {
            weatherList = weatherCacheDataSource.getWeatherFromCache()
        } catch (exception: Exception) {
            Log.i("MyTag", exception.message.toString())
        }
        if (weatherList.size > 0) {
            return weatherList
        } else {
            weatherList = getWeatherListFromDB()
            weatherCacheDataSource.saveWeatherFromCache(weatherList)
        }

        return weatherList
    }

}