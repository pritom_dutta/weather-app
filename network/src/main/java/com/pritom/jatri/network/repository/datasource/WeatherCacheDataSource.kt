package com.pritom.jatri.basicmovie.network.repository.movie.datasource

import com.pritom.jatri.network.model.weather.WeatherModel


interface WeatherCacheDataSource {
    suspend fun getWeatherFromCache(): List<WeatherModel>
    suspend fun saveWeatherFromCache(movies: List<WeatherModel>)
}