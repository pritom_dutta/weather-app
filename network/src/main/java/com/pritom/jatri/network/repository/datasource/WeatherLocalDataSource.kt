package com.pritom.jatri.basicmovie.network.repository.movie.datasource

import com.pritom.jatri.network.model.weather.WeatherModel


interface WeatherLocalDataSource {
    suspend fun getWeatherFromDB(): List<WeatherModel>
    suspend fun saveWeatherFromDB(movies: List<WeatherModel>)
    suspend fun clearAll()
}