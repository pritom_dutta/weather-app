package com.pritom.jatri.basicmovie.network.repository.movie.datasource

import com.pritom.jatri.network.model.WeatherList
import com.pritom.jatri.network.model.weather.Weather
import com.pritom.jatri.network.model.weather.WeatherModel
import retrofit2.Response

interface WeatherRemoteDataSource {
    suspend fun getWeatherList(): Response<WeatherList>

    suspend fun getWeatherDetails(lat:String,ln:String): Response<WeatherModel>
}