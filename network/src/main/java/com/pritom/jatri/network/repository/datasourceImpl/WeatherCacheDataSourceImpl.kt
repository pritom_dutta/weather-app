package com.pritom.jatri.basicmovie.network.repository.movie.datasourceImpl

import com.pritom.jatri.basicmovie.network.repository.movie.datasource.WeatherCacheDataSource
import com.pritom.jatri.network.model.weather.WeatherModel

class WeatherCacheDataSourceImpl : WeatherCacheDataSource {
    private var weatherList = ArrayList<WeatherModel>()

    override suspend fun getWeatherFromCache(): List<WeatherModel> = weatherList

    override suspend fun saveWeatherFromCache(movies: List<WeatherModel>) {
        weatherList.clear()
        weatherList = ArrayList(movies)
    }
}