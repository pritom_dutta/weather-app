package com.pritom.jatri.basicmovie.network.repository.movie.datasourceImpl

import com.pritom.jatri.basicmovie.network.db.dao.WeatherDao
import com.pritom.jatri.basicmovie.network.repository.movie.datasource.WeatherLocalDataSource
import com.pritom.jatri.network.model.weather.WeatherModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class WeatherLocalDataSourceImpl(private val weatherDao: WeatherDao) : WeatherLocalDataSource {

    override suspend fun getWeatherFromDB(): List<WeatherModel> = weatherDao.getWeather()

    override suspend fun saveWeatherFromDB(movies: List<WeatherModel>) {
        CoroutineScope(Dispatchers.IO).launch {
            weatherDao.saveWeatherList(movies)
        }
    }

    override suspend fun clearAll() {
        CoroutineScope(Dispatchers.IO).launch {
            weatherDao.deleteAllWeatherList()
        }
    }
}