package com.pritom.jatri.basicmovie.network.repository.movie.datasourceImpl

import com.pritom.jatri.basicmovie.network.api.TMDBService
import com.pritom.jatri.basicmovie.network.repository.movie.datasource.WeatherRemoteDataSource
import com.pritom.jatri.network.model.WeatherList
import com.pritom.jatri.network.model.weather.Weather
import com.pritom.jatri.network.model.weather.WeatherModel
import retrofit2.Response

class WeatherRemoteDataSourceImpl(
    private val mTMDBService: TMDBService,
    private val apiKey: String,
    private val lat: String,
    private val lon: String,
    private val cnt: String,
) : WeatherRemoteDataSource {

    override suspend fun getWeatherList(): Response<WeatherList> =
        mTMDBService.getWeather(apiKey, lat, lon, cnt)

    override suspend fun getWeatherDetails(lat:String,ln:String): Response<WeatherModel>
       = mTMDBService.getWeatherDetails(apiKey, lat, ln)


}